import { async, TestBed } from '@angular/core/testing';
import { ReportLibModule } from './report-lib.module';

describe('ReportLibModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReportLibModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(ReportLibModule).toBeDefined();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationResultLetterComponentComponent } from './application-result-letter-component.component';

describe('ApplicationResultLetterComponentComponent', () => {
  let component: ApplicationResultLetterComponentComponent;
  let fixture: ComponentFixture<ApplicationResultLetterComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationResultLetterComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationResultLetterComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

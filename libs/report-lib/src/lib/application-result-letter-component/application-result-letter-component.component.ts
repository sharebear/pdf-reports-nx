import {Component, Input, OnInit} from '@angular/core';
import {ApplicationResult, ApplicationResultLetter} from "@pdf-reports-nx/report-models";

@Component({
  selector: 'pdf-reports-nx-application-result-letter-component',
  templateUrl: './application-result-letter-component.component.html',
  styleUrls: ['./application-result-letter-component.component.css']
})
export class ApplicationResultLetterComponentComponent implements OnInit {

  @Input()
  model: ApplicationResultLetter;

  constructor() { }

  ngOnInit(): void {
  }

}

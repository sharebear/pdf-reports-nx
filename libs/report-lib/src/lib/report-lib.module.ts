import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationResultLetterComponentComponent } from './application-result-letter-component/application-result-letter-component.component';

@NgModule({
  imports: [CommonModule],
  exports: [
    ApplicationResultLetterComponentComponent
  ],
  declarations: [ApplicationResultLetterComponentComponent]
})
export class ReportLibModule {}

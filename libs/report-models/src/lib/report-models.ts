export enum ApplicationResult {
  APPROVED,
  DENIED
}

export interface ApplicationResultLetter {
  result: ApplicationResult
}

export interface ApplicationDetails {
  id: number;
  created: Date;
  letterData: ApplicationResultLetter;
}

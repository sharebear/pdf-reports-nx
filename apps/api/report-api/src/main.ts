/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import * as express from 'express';
import cors from 'cors';
import * as Keycloak from 'keycloak-connect';
import fetch from 'node-fetch';
import {ApplicationDetails, ApplicationResult} from "@pdf-reports-nx/report-models";

const app = express();

const applications: ApplicationDetails[] = [
  {
    id: 1,
    created: new Date(),
    letterData: {
      result: ApplicationResult.APPROVED
    }
  }
];

const keycloak = new Keycloak({}, {
    "realm": "pdf-reports",
    "bearer-only": true,
    "auth-server-url": "http://localhost:42080/auth/",
    "ssl-required": "external",
    "resource": "report-api",
    "confidential-port": 0
  }
);

app.use(cors());
app.use(keycloak.middleware());
app.get('/api/applications', keycloak.protect(), (req, res) => {
  res.send(JSON.stringify(applications));
});

app.get('/api/applications/:id', keycloak.protect(), (req, res) => {
  const applicationId = parseInt(req.params.id, 10);
  const application = applications.find(a => a.id === applicationId);
  res.send(JSON.stringify(application));
});

app.get('/api/applications/:id/as-pdf', keycloak.protect(), async (req, res) => {
  const applicationId = parseInt(req.params.id, 10);
  const htmlResult = await fetch(`http://localhost:4000/reports/applicationresponse/${applicationId}`, {
    headers: {
      'Authorization': `Bearer ${req.kauth.grant.access_token.token}`
    }
  });
  const reportAsHtml = await htmlResult.text();
  const htmlToPdfRequest = {
    bodyHtml: reportAsHtml
  };
  const pdfResult = await fetch('http://localhost:3000/', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(htmlToPdfRequest)
  });

  res.contentType('application/pdf');
  res.send(Buffer.from(await pdfResult.arrayBuffer()));
});

const port = process.env.port || 3333;
const server = app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}/api`);
});
server.on('error', console.error);

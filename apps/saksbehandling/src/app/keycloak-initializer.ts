import { KeycloakService } from 'keycloak-angular';

export function initializer(keycloak: KeycloakService): () => Promise<any> {
  return (): Promise<any> => keycloak.init(
    {
      config: {
        url: "http://localhost:42080/auth/",
        realm: "pdf-reports",
        clientId: "saksbehandling"
      },
      initOptions: {
        onLoad: "login-required"
      }
    }
  );
}

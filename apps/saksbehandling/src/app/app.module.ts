import { BrowserModule } from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import { ApplicationListComponent } from './application-list/application-list.component';
import { ApplicationEditorComponent } from './application-editor/application-editor.component';
import {RouterModule, Routes} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";
import {ReportLibModule} from "@pdf-reports-nx/report-lib";
import {initializer} from "./keycloak-initializer";
import {KeycloakAngularModule, KeycloakService} from "keycloak-angular";

const appRoutes: Routes = [
  {
    path: 'applications',
    component: ApplicationListComponent
  },
  {
    path: 'applications/:id',
    component: ApplicationEditorComponent
  },
  {
    path: '',
    redirectTo: 'applications',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [AppComponent, ApplicationListComponent, ApplicationEditorComponent],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpClientModule,
    ReportLibModule,
    KeycloakAngularModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      multi: true,
      deps: [KeycloakService]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

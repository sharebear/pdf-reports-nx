import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {ApplicationDetails} from "@pdf-reports-nx/report-models";
import {Observable} from "rxjs";
import {map, switchMap} from "rxjs/operators";

@Component({
  selector: 'pdf-reports-nx-application-editor',
  templateUrl: './application-editor.component.html',
  styleUrls: ['./application-editor.component.css']
})
export class ApplicationEditorComponent implements OnInit {

  application$: Observable<ApplicationDetails>;

  constructor(private activatedRoute: ActivatedRoute, private http: HttpClient) {
    this.application$ = activatedRoute.paramMap.pipe(
      map(paramMap => paramMap.get('id')),
      switchMap(applicationId => http.get<ApplicationDetails>(`/api/applications/${applicationId}`))
    );
  }

  ngOnInit(): void {
  }

  async downloadPdf(applicationId: number): void {

    const pdfBlob = await this.http.get(`/api/applications/${applicationId}/as-pdf`, {
      responseType: "blob"
    }).toPromise();
    const newBlob = new Blob([pdfBlob], {type: "application/pdf"});

    const data = window.URL.createObjectURL(newBlob);

    const link = document.createElement('a');
    link.href = data;
    link.download = 'myKoolPdf.pdf';
    link.click();

    link.remove();
  }

}

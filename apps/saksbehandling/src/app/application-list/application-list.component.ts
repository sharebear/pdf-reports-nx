import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {ApplicationDetails} from "@pdf-reports-nx/report-models";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'pdf-reports-nx-application-list',
  templateUrl: './application-list.component.html',
  styleUrls: ['./application-list.component.css']
})
export class ApplicationListComponent implements OnInit {
  applications$: Observable<ApplicationDetails[]>;

  constructor(http: HttpClient) {
    this.applications$ = http.get<ApplicationDetails[]>('/api/applications');
  }

  ngOnInit(): void {
  }

}

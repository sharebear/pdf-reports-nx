module.exports = {
  name: 'saksbehandling',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/saksbehandling',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};

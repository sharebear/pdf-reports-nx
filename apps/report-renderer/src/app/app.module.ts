import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { ApplicationResponseRendererComponent } from './application-response-renderer/application-response-renderer.component';
import {ReportLibModule} from "@pdf-reports-nx/report-lib";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [AppComponent, ApplicationResponseRendererComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    HttpClientModule,
    RouterModule.forRoot([
      {
        path: 'reports/applicationresponse/:id',
        component: ApplicationResponseRendererComponent
      }
    ], {initialNavigation: 'enabled'}),
    ReportLibModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}

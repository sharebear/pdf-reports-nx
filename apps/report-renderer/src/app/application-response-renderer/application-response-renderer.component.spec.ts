import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationResponseRendererComponent } from './application-response-renderer.component';

describe('ApplicationResponseRendererComponent', () => {
  let component: ApplicationResponseRendererComponent;
  let fixture: ComponentFixture<ApplicationResponseRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationResponseRendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationResponseRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

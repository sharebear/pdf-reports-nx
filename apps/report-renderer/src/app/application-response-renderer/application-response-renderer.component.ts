import { Component, OnInit } from '@angular/core';
import {map, switchMap} from "rxjs/operators";
import {ApplicationDetails, ApplicationResultLetter} from "@pdf-reports-nx/report-models";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs";

@Component({
  selector: 'pdf-reports-nx-application-response-renderer',
  templateUrl: './application-response-renderer.component.html',
  styleUrls: ['./application-response-renderer.component.css']
})
export class ApplicationResponseRendererComponent implements OnInit {
  letterData$: Observable<ApplicationResultLetter>;

  constructor(activatedRoute: ActivatedRoute, http: HttpClient) {
    this.letterData$ = activatedRoute.paramMap.pipe(
      map(paramMap => paramMap.get('id')),
      switchMap(applicationId => http.get<ApplicationDetails>(`http://localhost:3333/api/applications/${applicationId}`)),
      map(foo => foo.letterData)
    );
  }

  ngOnInit(): void {
  }

}

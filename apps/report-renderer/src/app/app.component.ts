import { Component } from '@angular/core';

@Component({
  selector: 'pdf-reports-nx-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'report-renderer';
}

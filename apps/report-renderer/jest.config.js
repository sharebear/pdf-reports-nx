module.exports = {
  name: 'report-renderer',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/report-renderer',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
